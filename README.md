# Microservices - app2

This app is part of the Microservices project.
app2 send messages to rabbitmq
For this, app2 has a StatusUpdate message and a StatusUpdateHandler handler, in the same format as in the app1. 

## Setup
See Microservices repository documentation : https://gitlab.com/tiatony/microservices